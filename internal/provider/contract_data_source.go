// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"context"
	"fmt"
	"regexp"

	fsc "terraform-provider-fsc/api"

	"github.com/hashicorp/terraform-plugin-framework-validators/stringvalidator"
	"github.com/hashicorp/terraform-plugin-framework/attr"
	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/datasource/schema"
	"github.com/hashicorp/terraform-plugin-framework/schema/validator"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"
)

var _ datasource.DataSource = &ContractDataSource{}

func NewContractDataSource() datasource.DataSource {
	return &ContractDataSource{}
}

type ContractDataSource struct {
	client *fsc.Client
}

type ContractDataSourceModel struct {
	Iv          types.String `tfsdk:"iv"`
	ContentHash types.String `tfsdk:"content_hash"`
	Peers       types.Map    `tfsdk:"peers"`
	Content     types.Object `tfsdk:"content"`
	HasRejected types.Bool   `tfsdk:"has_rejected"`
	HasRevoked  types.Bool   `tfsdk:"has_revoked"`
	HasAccepted types.Bool   `tfsdk:"has_accepted"`
	Signatures  types.Object `tfsdk:"signatures"`
}

func (d *ContractDataSource) Metadata(ctx context.Context, req datasource.MetadataRequest, resp *datasource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_contract"
}

func (d *ContractDataSource) Schema(ctx context.Context, req datasource.SchemaRequest, resp *datasource.SchemaResponse) {
	resp.Schema = schema.Schema{
		// This description is used by the documentation generator and the language server.
		MarkdownDescription: "Fetches a FSC contract",

		Attributes: map[string]schema.Attribute{
			"iv": schema.StringAttribute{
				MarkdownDescription: "UUID of Contract",
				Required:            true,
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$`),
						"must be a valid UUID",
					),
				},
			},
			"content_hash": schema.StringAttribute{
				MarkdownDescription: "Content Hash",
				Computed:            true,
			},
			"peers": schema.MapAttribute{
				MarkdownDescription: "Map of PeerIDs and Peers",
				ElementType: types.MapType{
					ElemType: types.StringType,
				},
				Computed: true,
			},
			"content": schema.ObjectAttribute{
				MarkdownDescription: "Contract Content",
				AttributeTypes: map[string]attr.Type{
					"iv":       types.StringType,
					"group_id": types.StringType,
					"validity": types.MapType{ElemType: types.Int64Type},
					"grants": types.ListType{ElemType: types.ObjectType{AttrTypes: map[string]attr.Type{
						"type":      types.StringType,
						"hash":      types.StringType,
						"delegator": types.MapType{ElemType: types.StringType},
						"directory": types.MapType{ElemType: types.StringType},
						"outway":    types.MapType{ElemType: types.StringType},
						"service":   types.MapType{ElemType: types.StringType},
					}}},
					"hash_algorithm": types.StringType,
					"created_at":     types.Int64Type,
				},
				Computed: true,
			},
			"has_rejected": schema.BoolAttribute{
				MarkdownDescription: "True if peer has rejected the contract",
				Computed:            true,
			},
			"has_revoked": schema.BoolAttribute{
				MarkdownDescription: "True if peer has revoked the contract",
				Computed:            true,
			},
			"has_accepted": schema.BoolAttribute{
				MarkdownDescription: "True if peer has accepted the contract",
				Computed:            true,
			},
			"signatures": schema.ObjectAttribute{
				MarkdownDescription: "The accept, reject and revoke signatures",
				AttributeTypes: map[string]attr.Type{
					"accept": types.MapType{ElemType: types.ObjectType{AttrTypes: map[string]attr.Type{
						"peer":      types.MapType{ElemType: types.StringType},
						"signed_at": types.Int64Type,
					}},
					},
					"reject": types.MapType{ElemType: types.ObjectType{AttrTypes: map[string]attr.Type{
						"peer":      types.MapType{ElemType: types.StringType},
						"signed_at": types.Int64Type,
					}},
					},
					"revoke": types.MapType{ElemType: types.ObjectType{AttrTypes: map[string]attr.Type{
						"peer":      types.MapType{ElemType: types.StringType},
						"signed_at": types.Int64Type,
					}},
					},
				},
				Computed: true,
			},
		},
	}
}

func (d *ContractDataSource) Configure(ctx context.Context, req datasource.ConfigureRequest, resp *datasource.ConfigureResponse) {
	// Prevent panic if the provider has not been configured.
	if req.ProviderData == nil {
		return
	}

	client, ok := req.ProviderData.(*fsc.Client)

	if !ok {
		resp.Diagnostics.AddError(
			"Unexpected Data Source Configure Type",
			fmt.Sprintf("Expected *fsc.Client, got: %T. Please report this issue to the provider developers.", req.ProviderData),
		)

		return
	}

	d.client = client
}

func (d *ContractDataSource) Read(ctx context.Context, req datasource.ReadRequest, resp *datasource.ReadResponse) {
	var plan, state ContractDataSourceModel

	// Read Terraform configuration data into the model
	resp.Diagnostics.Append(req.Config.Get(ctx, &plan)...)
	resp.Diagnostics.Append(resp.State.Get(ctx, &state)...)

	if resp.Diagnostics.HasError() {
		return
	}

	contract, err := d.client.GetContract(plan.Iv.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error retrieving Contract",
			"Could not enumerate contracts: "+err.Error(),
		)
		return
	}

	if contract == nil {
		resp.Diagnostics.AddError(
			"Error retrieving Contract",
			"Could not find a matching Contract",
		)
		return
	}

	convertedContract, err := convertContract(contract)
	if err != nil {
		resp.Diagnostics.AddError(
			"Error retrieving Contracts",
			"Failed to process Contract: "+err.Error(),
		)
		return
	}

	state.ContentHash = convertedContract.ContentHash
	state.Peers = convertedContract.Peers
	state.Content = convertedContract.Content
	state.HasRejected = convertedContract.HasRejected
	state.HasRevoked = convertedContract.HasRevoked
	state.HasAccepted = convertedContract.HasAccepted
	state.Signatures = convertedContract.Signatures

	tflog.Trace(ctx, "read FSC contract data source")

	// Save data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &state)...)
}
