package provider

import (
	"context"
	"os"
	"regexp"

	"github.com/hashicorp/terraform-plugin-framework-validators/stringvalidator"
	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/provider"
	"github.com/hashicorp/terraform-plugin-framework/provider/schema"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/schema/validator"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"
	fsc "terraform-provider-fsc/api"
)

var _ provider.Provider = &FscProvider{}

type FscProvider struct {
	// version is set to the provider version on release, "dev" when the
	// provider is built and ran locally, and "test" when running acceptance
	// testing.
	version string
}

type FscProviderModel struct {
	GroupId              types.String `tfsdk:"group_id"`
	ControllerEndpoint   types.String `tfsdk:"controller_endpoint"`
	ManagerEndpoint      types.String `tfsdk:"manager_endpoint"`
	CaCerts              types.String `tfsdk:"ca_certs"`
	ControllerClientCert types.String `tfsdk:"controller_client_cert"`
	ControllerClientKey  types.String `tfsdk:"controller_client_key"`
	ManagerClientCert    types.String `tfsdk:"manager_client_cert"`
	ManagerClientKey     types.String `tfsdk:"manager_client_key"`
}

func (p *FscProvider) Metadata(ctx context.Context, req provider.MetadataRequest, resp *provider.MetadataResponse) {
	resp.TypeName = "fsc"
	resp.Version = p.version
}

func (p *FscProvider) Schema(ctx context.Context, req provider.SchemaRequest, resp *provider.SchemaResponse) {
	resp.Schema = schema.Schema{
		Attributes: map[string]schema.Attribute{
			"group_id": schema.StringAttribute{
				MarkdownDescription: "FSC Group ID. May also be provided via FSC_GROUP_ID environment variable.",
				Optional:            true,
			},
			"controller_endpoint": schema.StringAttribute{
				MarkdownDescription: "Address of controller's internal API. May also be provided via FSC_CONTROLLER_ENDPOINT environment variable.",
				Optional:            true,
				Validators: []validator.String{
					stringvalidator.LengthBetween(8, 256),
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^https://\S+$`),
						"must be a valid URL",
					),
				},
			},
			"manager_endpoint": schema.StringAttribute{
				MarkdownDescription: "Address of manager's internal API. May also be provided via FSC_MANAGER_ENDPOINT environment variable.",
				Optional:            true,
				Validators: []validator.String{
					stringvalidator.LengthBetween(8, 256),
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^https://\S+$`),
						"must be a valid URL",
					),
				},
			},
			"ca_certs": schema.StringAttribute{
				MarkdownDescription: "PEM encoded public key(s) of CA(s). May also be provided via FSC_CA_CERTIFICATES environment variable.",
				Optional:            true,
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^-----BEGIN CERTIFICATE-----`),
						"must be a valid PEM encoded certificate",
					),
				},
			},
			"controller_client_cert": schema.StringAttribute{
				MarkdownDescription: "PEM encoded public key of client. May also be provided via FSC_CLIENT_CERTIFICATE environment variable.",
				Optional:            true,
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^-----BEGIN CERTIFICATE-----`),
						"must be a valid PEM encoded certificate",
					),
				},
			},
			"controller_client_key": schema.StringAttribute{
				MarkdownDescription: "PEM encoded private key of client. May also be provided via FSC_CONTROLLER_CLIENT_KEY environment variable.",
				Optional:            true,
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^-----BEGIN (?:RSA\s)?PRIVATE KEY-----`),
						"must be a valid unencrypted PEM encoded private key",
					),
				},
			},
			"manager_client_cert": schema.StringAttribute{
				MarkdownDescription: "PEM encoded public key of client. May also be provided via FSC_MANAGER_CLIENT_CERTIFICATE environment variable.",
				Optional:            true,
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^-----BEGIN CERTIFICATE-----`),
						"must be a valid PEM encoded certificate",
					),
				},
			},
			"manager_client_key": schema.StringAttribute{
				MarkdownDescription: "PEM encoded private key of client. May also be provided via FSC_MANAGER_CLIENT_KEY environment variable.",
				Optional:            true,
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(
							`^-----BEGIN (?:RSA\s)?PRIVATE KEY-----`),
						"must be a valid unencrypted PEM encoded private key",
					),
				},
			},
		},
	}
}

func (p *FscProvider) Configure(ctx context.Context, req provider.ConfigureRequest, resp *provider.ConfigureResponse) {
	var config FscProviderModel

	resp.Diagnostics.Append(req.Config.Get(ctx, &config)...)

	groupId := os.Getenv("FSC_GROUP_ID")
	controllerEndpoint := os.Getenv("FSC_CONTROLLER_ENDPOINT")
	managerEndpoint := os.Getenv("FSC_MANAGER_ENDPOINT")
	CaCertsPem := os.Getenv("FSC_CA_CERTS")
	controllerClientCertPem := os.Getenv("FSC_CONTROLLER_CLIENT_CERT")
	controllerClientKeyPem := os.Getenv("FSC_CONTROLLER_CLIENT_KEY")
	managerClientCertPem := os.Getenv("FSC_MANAGER_CLIENT_CERT")
	managerClientKeyPem := os.Getenv("FSC_MANAGER_CLIENT_KEY")

	if !config.GroupId.IsNull() {
		groupId = config.GroupId.ValueString()
	}
	if !config.ControllerEndpoint.IsNull() {
		controllerEndpoint = config.ControllerEndpoint.ValueString()
	}
	if !config.ManagerEndpoint.IsNull() {
		managerEndpoint = config.ManagerEndpoint.ValueString()
	}
	if !config.CaCerts.IsNull() {
		CaCertsPem = config.CaCerts.ValueString()
	}
	if !config.ControllerClientCert.IsNull() {
		controllerClientCertPem = config.ControllerClientCert.ValueString()
	}
	if !config.ControllerClientKey.IsNull() {
		controllerClientKeyPem = config.ControllerClientKey.ValueString()
	}
	if !config.ManagerClientCert.IsNull() {
		managerClientCertPem = config.ManagerClientCert.ValueString()
	}
	if !config.ManagerClientKey.IsNull() {
		managerClientKeyPem = config.ManagerClientKey.ValueString()
	}

	if len(groupId) == 0 {
		resp.Diagnostics.AddAttributeError(
			path.Root("group_id"),
			"Missing FSC Group ID",
			"The FSC APIs make use of the Group ID to be able to differentiate between entities. "+
				"The value can be set in the configuration or use the FSC_GROUP_ID environment variable. ",
		)
	}
	if len(controllerEndpoint) == 0 {
		resp.Diagnostics.AddAttributeError(
			path.Root("controller_endpoint"),
			"Missing FSC Controller API Endpoint",
			"The provider needs to be able to communicate with the FSC controller's internal API. "+
				"The value can be set in the configuration or use the FSC_CONTROLLER_ENDPOINT environment variable. ",
		)
	}
	if len(managerEndpoint) == 0 {
		resp.Diagnostics.AddAttributeError(
			path.Root("manager_endpoint"),
			"Missing FSC Manager API Endpoint",
			"The provider needs to be able to communicate with the FSC manager's internal API. "+
				"The value can be set in the configuration or use the FSC_MANAGER_ENDPOINT environment variable. ",
		)
	}
	if len(CaCertsPem) == 0 {
		resp.Diagnostics.AddAttributeError(
			path.Root("ca_certs"),
			"Missing CA certificate(s)",
			"All communication is based on mTLS, and thus (a) CA certificate(s) is/are required to be able to verify peers."+
				"The value can be set in the configuration or use the FSC_CA_CERTS environment variable. ",
		)
	}
	if len(controllerClientCertPem) == 0 {
		resp.Diagnostics.AddAttributeError(
			path.Root("controller_client_cert"),
			"Missing client certificate",
			"All communication is based on mTLS, and thus clients are supposed to represent themselves using a client certificate."+
				"The value can be set in the configuration or use the FSC_CONTROLLER_CLIENT_CERT environment variable. ",
		)
	}
	if len(controllerClientKeyPem) == 0 {
		resp.Diagnostics.AddAttributeError(
			path.Root("controller_client_key"),
			"Missing client key",
			"All communication is based on mTLS, and thus clients are supposed to be able to encrypt requests, using a dedicated private key."+
				"The value can be set in the configuration or use the FSC_CONTROLLER_CLIENT_KEY environment variable. ",
		)
	}
	if len(managerClientCertPem) == 0 {
		resp.Diagnostics.AddAttributeError(
			path.Root("manager_client_cert"),
			"Missing client certificate",
			"All communication is based on mTLS, and thus clients are supposed to represent themselves using a client certificate."+
				"The value can be set in the configuration or use the FSC_MANAGER_CLIENT_CERT environment variable. ",
		)
	}
	if len(managerClientKeyPem) == 0 {
		resp.Diagnostics.AddAttributeError(
			path.Root("manager_client_key"),
			"Missing client key",
			"All communication is based on mTLS, and thus clients are supposed to be able to encrypt requests, using a dedicated private key."+
				"The value can be set in the configuration or use the FSC_MANAGER_CLIENT_KEY environment variable. ",
		)
	}

	if resp.Diagnostics.HasError() {
		return
	}

	ctx = tflog.SetField(ctx, "groupId", groupId)
	ctx = tflog.SetField(ctx, "controllerEndpoint", controllerEndpoint)
	ctx = tflog.SetField(ctx, "managerEndpoint", managerEndpoint)

	tflog.Debug(ctx, "Creating FSC API client")

	controllerCerts := []string{CaCertsPem, controllerClientCertPem, controllerClientKeyPem}
	managerCerts := []string{CaCertsPem, managerClientCertPem, managerClientKeyPem}

	client, err := fsc.NewClient(groupId, controllerEndpoint, managerEndpoint, controllerCerts, managerCerts)
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to Create FSC API Client",
			"An unexpected error occurred when creating the FSC API client. "+
				"If the error is not clear, please contact the provider developers.\n\n"+
				"FSC Client Error: "+err.Error(),
		)
		return
	}

	resp.DataSourceData = client
	resp.ResourceData = client

	tflog.Info(ctx, "Configured FSC client", map[string]any{"success": true})
}

func (p *FscProvider) Resources(ctx context.Context) []func() resource.Resource {
	return []func() resource.Resource{
		NewServiceResource,
		NewContractResource,
	}
}

func (p *FscProvider) DataSources(ctx context.Context) []func() datasource.DataSource {
	return []func() datasource.DataSource{
		NewServiceDataSource,
		NewContractDataSource,
		NewOutwayDataSource,
		NewInwayDataSource,
	}
}

func New(version string) func() provider.Provider {
	return func() provider.Provider {
		return &FscProvider{
			version: version,
		}
	}
}
