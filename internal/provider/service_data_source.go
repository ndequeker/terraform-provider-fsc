// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"context"
	"fmt"
	"regexp"

	"github.com/hashicorp/terraform-plugin-framework-validators/stringvalidator"
	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/datasource/schema"
	"github.com/hashicorp/terraform-plugin-framework/schema/validator"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"
	fsc "terraform-provider-fsc/api"
)

var _ datasource.DataSource = &ServiceDataSource{}

func NewServiceDataSource() datasource.DataSource {
	return &ServiceDataSource{}
}

type ServiceDataSource struct {
	client *fsc.Client
}

type ServiceDataSourceModel struct {
	Name         types.String `tfsdk:"name"`
	EndpointUrl  types.String `tfsdk:"endpoint_url"`
	InwayAddress types.String `tfsdk:"inway_address"`
}

func (d *ServiceDataSource) Metadata(ctx context.Context, req datasource.MetadataRequest, resp *datasource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_service"
}

func (d *ServiceDataSource) Schema(ctx context.Context, req datasource.SchemaRequest, resp *datasource.SchemaResponse) {
	resp.Schema = schema.Schema{
		// This description is used by the documentation generator and the language server.
		MarkdownDescription: "Fetches a FSC service",

		Attributes: map[string]schema.Attribute{
			"name": schema.StringAttribute{
				MarkdownDescription: "Service name",
				Required:            true,
				Validators: []validator.String{
					stringvalidator.RegexMatches(
						regexp.MustCompile(`^[a-zA-Z0-9-_.]+$`),
						"must contain only letters, numbers, -, _ and .",
					),
				},
			},
			"endpoint_url": schema.StringAttribute{
				Computed:            true,
				MarkdownDescription: "Endpoint URL",
			},
			"inway_address": schema.StringAttribute{
				Computed:            true,
				MarkdownDescription: "Inway Address",
			},
		},
	}
}

func (d *ServiceDataSource) Configure(ctx context.Context, req datasource.ConfigureRequest, resp *datasource.ConfigureResponse) {
	// Prevent panic if the provider has not been configured.
	if req.ProviderData == nil {
		return
	}

	client, ok := req.ProviderData.(*fsc.Client)

	if !ok {
		resp.Diagnostics.AddError(
			"Unexpected Data Source Configure Type",
			fmt.Sprintf("Expected *fsc.Client, got: %T. Please report this issue to the provider developers.", req.ProviderData),
		)

		return
	}

	d.client = client
}

func (d *ServiceDataSource) Read(ctx context.Context, req datasource.ReadRequest, resp *datasource.ReadResponse) {
	var plan, state ServiceDataSourceModel

	// Read Terraform configuration data into the model
	resp.Diagnostics.Append(req.Config.Get(ctx, &plan)...)
	resp.Diagnostics.Append(resp.State.Get(ctx, &state)...)

	if resp.Diagnostics.HasError() {
		return
	}

	service, err := d.client.GetService(plan.Name.ValueString())
	if err != nil {
		resp.Diagnostics.AddError(
			"Error retrieving Service",
			"Could not enumerate services: "+err.Error(),
		)
		return
	}

	if service == nil {
		resp.Diagnostics.AddError(
			"Error retrieving Service",
			"Could not find a matching Service",
		)
		return
	}

	state.Name = types.StringValue(service.Name)
	state.EndpointUrl = types.StringValue(service.EndpointUrl)
	state.InwayAddress = types.StringValue(service.InwayAddress)

	tflog.Trace(ctx, "read a service data source")

	// Save data into Terraform state
	resp.Diagnostics.Append(resp.State.Set(ctx, &state)...)
}
