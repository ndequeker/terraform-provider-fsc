<p align="center">
    <img src="https://docs.fsc.nlx.io/img/logo_fsc-nlx.png" height="150" alt="FSC logo">
</p>

---

# Terraform FSC Provider

Tested against main branch, on Terraform 1.6.6

To use the provider; run `make install` in conjunction with the following config:

```hcl
terraform {
  required_providers {
    fsc = {
      source  = "terraform.local/local/fsc"
      version = "0.1.0"
    }
  }
}

provider "fsc" {
  group_id               = "fsc-demo"
  controller_endpoint    = "https://controller:444"
  manager_endpoint       = "https://manager:443"
  ca_certs               = file("/certs/cacerts.pem")
  controller_client_cert = file("/certs/controller_client_cert.pem")
  controller_client_key  = file("/certs/controller_client_key.pem")
  manager_client_cert    = file("/certs/manager_client_cert.pem")
  manager_client_key     = file("/certs/manager_client_key.pem")
}
```

Alternatively these variables can be used to configure the provider:
```shell
export FSC_GROUP_ID=fsc-demo
export FSC_CONTROLLER_ENDPOINT=https://controller:444
export FSC_MANAGER_ENDPOINT=https://manager:443
export FSC_CA_CERTS=$(cat ~/cacerts.pem)
export FSC_CONTROLLER_CLIENT_CERT=$(cat ~/controller_client_cert.pem)
export FSC_CONTROLLER_CLIENT_KEY=$(cat ~/controller_client_key.pem)
export FSC_MANAGER_CLIENT_CERT=$(cat ~/manager_client_cert.pem)
export FSC_MANAGER_CLIENT_KEY=$(cat ~/manager_client_key.pem)
```

### Generate Documentation

Run `make docs` from the project root to regenerate the latest provider documentation

## Testing

Deploy FSC locally instance by following https://docs.nlx.io/try-nlx/docker/setup-your-environment/

Afterwards, tests can be run from your editor or via CLI, using `make test`
