package fsc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func (c *Client) CreateService(serviceName, endpointUrl, inwayAddress string) error {
	request := Service{
		Name:         serviceName,
		EndpointUrl:  endpointUrl,
		InwayAddress: inwayAddress,
	}

	body, err := json.Marshal(request)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/services", c.ControllerEndpoint), bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	req.Header.Set("content-type", "application/json")

	_, err = c.doControllerRequest(req)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) GetServices() ([]Service, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/services", c.ControllerEndpoint), nil)
	if err != nil {
		return nil, err
	}

	body, err := c.doControllerRequest(req)
	if err != nil {
		return nil, err
	}

	response := GetServicesResponse{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return response.Services, nil
}

func (c *Client) GetService(serviceId string) (*Service, error) {
	services, err := c.GetServices()
	if err != nil {
		return nil, err
	}

	for _, service := range services {
		if service.Name == serviceId {
			return &service, nil
		}
	}

	return nil, nil
}

func (c *Client) UpdateService(serviceName, endpointUrl, inwayAddress string) error {
	request := Service{
		EndpointUrl:  endpointUrl,
		InwayAddress: inwayAddress,
	}

	body, err := json.Marshal(request)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/services/%s", c.ControllerEndpoint, serviceName), bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	req.Header.Set("content-type", "application/json")

	_, err = c.doControllerRequest(req)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) DeleteService(serviceName string) error {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/services/%s", c.ControllerEndpoint, serviceName), nil)
	if err != nil {
		return err
	}

	_, err = c.doControllerRequest(req)
	if err != nil {
		return err
	}

	return nil
}
