---
page_title: "fsc_contract Data Source - terraform-provider-fsc"
subcategory: ""
description: |-
  Fetches a FSC contract
---

# fsc_contract (Data Source)


Fetches a FSC contract

## Example Usage

```terraform
data "fsc_contract" "example" {
  id = "fec3c317-ea47-4ced-8f09-0a8f0dad29f0"
}
```

<!-- schema generated by tfplugindocs -->
## Schema

### Required

- `id` (String) UUID of Contract

### Read-Only

- `content` (Object) Contract Content (see [below for nested schema](#nestedatt--content))
- `content_hash` (String) Content Hash
- `has_accepted` (Boolean) True if peer has accepted the contract
- `has_rejected` (Boolean) True if peer has rejected the contract
- `has_revoked` (Boolean) True if peer has revoked the contract
- `peers` (Map of Map of String) Map of PeerIDs and Peers
- `signatures` (Object) The accept, reject and revoke signatures (see [below for nested schema](#nestedatt--signatures))

<a id="nestedatt--content"></a>
### Nested Schema for `content`

Read-Only:

- `created_at` (Number)
- `grants` (List of Object) (see [below for nested schema](#nestedobjatt--content--grants))
- `group_id` (String)
- `hash_algorithm` (String)
- `id` (String)
- `validity` (Map of Number)

<a id="nestedobjatt--content--grants"></a>
### Nested Schema for `content.grants`

Read-Only:

- `delegator` (Map of String)
- `directory` (Map of String)
- `hash` (String)
- `outway` (Map of String)
- `service` (Map of String)
- `type` (String)



<a id="nestedatt--signatures"></a>
### Nested Schema for `signatures`

Read-Only:

- `accept` (Map of Object) (see [below for nested schema](#nestedobjatt--signatures--accept))
- `reject` (Map of Object) (see [below for nested schema](#nestedobjatt--signatures--reject))
- `revoke` (Map of Object) (see [below for nested schema](#nestedobjatt--signatures--revoke))

<a id="nestedobjatt--signatures--accept"></a>
### Nested Schema for `signatures.accept`

Read-Only:

- `peer` (Map of String)
- `signed_at` (Number)


<a id="nestedobjatt--signatures--reject"></a>
### Nested Schema for `signatures.reject`

Read-Only:

- `peer` (Map of String)
- `signed_at` (Number)


<a id="nestedobjatt--signatures--revoke"></a>
### Nested Schema for `signatures.revoke`

Read-Only:

- `peer` (Map of String)
- `signed_at` (Number)
