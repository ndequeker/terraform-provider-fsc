package fsc

func GrantServicePublication(directoryPeerId, servicePeerId, serviceName, serviceProtocol string) Grant {
	var grant Grant
	grant.FromGrantServicePublication(ServicePublicationGrant{
		Directory: DirectoryPeer{
			PeerId: directoryPeerId,
		},
		Service: ServicePublicationPeer{
			Name:     serviceName,
			PeerId:   servicePeerId,
			Protocol: Protocol(serviceProtocol),
		},
	})

	return grant
}

func GrantServiceConnection(publicKeyThumbprint, outwayPeerId, servicePeerId, serviceName string) Grant {
	var grant Grant
	grant.FromGrantServiceConnection(ServiceConnectionGrant{
		Outway: OutwayPeer{
			PublicKeyThumbprint: publicKeyThumbprint,
			PeerId:              outwayPeerId,
		},
		Service: ServicePeer{
			Name:   serviceName,
			PeerId: servicePeerId,
		},
	})

	return grant
}

func GrantDelegatedServicePublication(delegatorPeerId, directoryPeerId, servicePeerId, serviceName, serviceProtocol string) Grant {
	var grant Grant
	grant.FromGrantDelegatedServicePublication(DelegatedServicePublicationGrant{
		Delegator: DelegatorPeer{
			PeerId: delegatorPeerId,
		},
		Directory: DirectoryPeer{
			PeerId: directoryPeerId,
		},
		Service: ServicePublicationPeer{
			Name:     serviceName,
			PeerId:   servicePeerId,
			Protocol: Protocol(serviceProtocol),
		},
	})

	return grant
}

func GrantDelegatedServiceConnection(delegatorPeerId, publicKeyThumbprint, outwayPeerId, servicePeerId, serviceName string) Grant {
	var grant Grant
	grant.FromGrantDelegatedServiceConnection(DelegatedServiceConnectionGrant{
		Delegator: DelegatorPeer{
			PeerId: delegatorPeerId,
		},
		Outway: OutwayPeer{
			PublicKeyThumbprint: publicKeyThumbprint,
			PeerId:              outwayPeerId,
		},
		Service: ServicePeer{
			Name:   serviceName,
			PeerId: servicePeerId,
		},
	})

	return grant
}
