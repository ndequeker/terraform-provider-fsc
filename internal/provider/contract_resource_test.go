// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

package provider

import (
	"fmt"
	"testing"

	_ "github.com/hashicorp/terraform-plugin-testing/helper/resource"
)

func TestAccContractResource(t *testing.T) {
	// TODO: add test that verifies proper creation, and errors on updates/removals
	// See https://github.com/hashicorp/terraform-plugin-testing/issues/85
}

func testAccContractResourceConfig() string {
	return fmt.Sprintf(`
    TODO
`)
}
